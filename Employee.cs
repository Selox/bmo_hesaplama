using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProjeX
{
    public class Employee
    {
        double original_salary = 4500;
        double multiplier = 1;
        List<Bilgi> employeeList = new List<Bilgi>();
        int staffNumber = 1;
        public int StaffID
        {
            get { return staffNumber; }
            set { staffNumber = value; }
        }
        public List<Bilgi> degisken()
        {
            return employeeList;
        }

        public int staffIDofSelectedIndex(int indexX)//Seçili elemanın ID sini döndürür
        {
            return employeeList[indexX].identity;
        }

        public int indexOfStaffID(int idX)
        {
            int index = -1;

            for (int i = 0; i < employeeList.Count; i++)
            {
                if (employeeList[i].identity == idX)
                {
                    index = i;
                }
            }
            return index;
        }

        public void Staff_add( string Isim, string Soyisim, int YasSehir, int CalSehir, int Tecrübe, bool İngilizce, int ekdiller, 
            int Ogrenim, int Tip, bool evlilik, bool Eşin_İşi, int cocuksayisi, string Cocuk_1, string Cocuk_2, bool Okuyan1, bool Okuyan2)
        {
            employeeList.Add(new Bilgi
            {
                identity = staffNumber,
                name = Isim,
                surname = Soyisim,
                localCity = YasSehir,
                workCity = CalSehir,
                experince = Tecrübe,
                English = İngilizce,
                additionalLanguages = ekdiller,
                educationalRank = Ogrenim,
                jobType = Tip,
                marriage = evlilik,
                workingpartner = Eşin_İşi,
                childrencount = cocuksayisi,
                Child1 = Cocuk_1,
                Child2 = Cocuk_2,
                Child1_Educated = Okuyan1,
                Child2_Educated = Okuyan2,
                salary = original_salary*Multiplier_Calculator(Tecrübe,İngilizce,ekdiller,Ogrenim,Tip,evlilik,Eşin_İşi,cocuksayisi,Okuyan1,Okuyan2, agecalculator(Cocuk_1), agecalculator(Cocuk_2),YasSehir)
            });
            staffNumber++;
        }
        //TAMAMLANDI
        public void Staff_update(int index,string Isim, string Soyisim, int YasSehir, int CalSehir, int Tecrübe, bool İngilizce, int ekdiller,
           int Ogrenim, int Tip, bool evlilik, bool Eşin_İşi, int cocuksayisi, string Cocuk_1, string Cocuk_2, bool Okuyan1, bool Okuyan2)//TAMAMLANDI //TEST EDİLMEDİ
        {
            employeeList[index].name = Isim;
            employeeList[index].surname = Soyisim;
            employeeList[index].localCity = YasSehir;
            employeeList[index].workCity = CalSehir;
            employeeList[index].experince = Tecrübe;
            employeeList[index].English = İngilizce;
            employeeList[index].additionalLanguages = ekdiller;
            employeeList[index].educationalRank = Ogrenim;
            employeeList[index].jobType = Tip;
            employeeList[index].marriage = evlilik;
            employeeList[index].workingpartner = Eşin_İşi;
            employeeList[index].childrencount = cocuksayisi;
            employeeList[index].Child1 = Cocuk_1;
            employeeList[index].Child2 = Cocuk_2;
            employeeList[index].Child1_Educated = Okuyan1;
            employeeList[index].Child2_Educated = Okuyan2;
            employeeList[index].salary = original_salary * Multiplier_Calculator(Tecrübe, İngilizce, ekdiller, Ogrenim, Tip, evlilik, Eşin_İşi, cocuksayisi, Okuyan1, Okuyan2, agecalculator(Cocuk_1), agecalculator(Cocuk_2),YasSehir);
        }
        //SANIRIM BİR SORUN VAR

        public double Multiplier_Calculator(int exp,bool eng,int count,int eduRank,int jobtype,bool marry,bool workpart,int Child_count,bool Edu_1,bool Edu_2,int Child1,int Child2,int City)
        {
            if (Child_count >= 2)
            {
                multiplier = multiplier + Experince_Multiplier(exp) + Language_Multiplier(eng, count) + Education_Multiplier(eduRank) + Job_Multiplier(jobtype) + Marriage_Multiplier(marry, workpart) +
                    Children_Multiplier(Edu_1, Child1) + Children_Multiplier(Edu_2, Child2) + City_Multiplier(City);
            }
            else if (Child_count == 1)
            {
                multiplier = multiplier + Experince_Multiplier(exp) + Language_Multiplier(eng, count) + Education_Multiplier(eduRank) + Job_Multiplier(jobtype) + Marriage_Multiplier(marry, workpart) +
                    Children_Multiplier(Edu_1, Child1) + City_Multiplier(City);
            }
            else
            {
                multiplier = multiplier + Experince_Multiplier(exp) + Language_Multiplier(eng, count) + Education_Multiplier(eduRank) + Job_Multiplier(jobtype) + Marriage_Multiplier(marry, workpart) +
                   + City_Multiplier(City);
            }
            return multiplier;
        }
        //TAMAMLANDI

        public string Printer(int number)
        {
            number--;
            string employee = String.Empty;
            employee = employeeList[number].identity.ToString() + " " + employeeList[number].name + " " + employeeList[number].surname + " " + cityfinder(employeeList[number].localCity);
            if (employeeList[number].marriage)
            { 
                employee += " Evli";
            }
            else
            {
                employee += " Bekar";
            }
            employee += " " + employeeList[number].salary;
            return employee;
        }//HATALI EKSİKLER VAR

        /*public string file_Printer()//YAZDIRMA FONKSİYONU ÇALIŞIYORDU!
        {
            string StrinG = String.Empty;
            StrinG += b.identity.ToString() + ",";
            StrinG += b.name + ",";
            StrinG += b.surname + ",";
            StrinG += b.localCity.ToString() + ",";
            if (b.marriage)
                StrinG += "1,";
            else
                StrinG += "0,";
            StrinG += b.salary.ToString();
            return StrinG;
        }*/

        public double Experince_Multiplier(int work_exp)
        {
            if (work_exp < 2)
            { return 0; }
            else if (work_exp < 5)
            { return 0.6; }
            else if (work_exp < 10)
            { return 1; }
            else if (work_exp < 15)
            { return 1.2; }
            else if (work_exp < 20)
            { return 1.35; }
            else
            { return 1.50; }
        }//TAMAMLANDI

        public double Language_Multiplier(bool eng,int count)//TAMAMLANDI
        {
            if (eng == true)
            { return 0.2 + (0.05 * count); }
            else
            { return 0 + (0.05 * count); }
        }

        public double Education_Multiplier(int eduRank)//TAMAMLANDI
        {
            if (eduRank == 0)
            { return 0.10; }
            else if (eduRank == 1)
            { return 0.30; }
            else if (eduRank == 2)
            { return 0.35; }
            else if (eduRank == 3)
            { return 0.05; }
            else
            { return 0.15; }
        }

        public double Job_Multiplier(int jobtype)//TAMAMLANDI
        {
            if (jobtype == 0)
            { return 0.5; }
            else if (jobtype == 1)
            { return 0.75; }
            else if (jobtype == 2)
            { return 0.85; }
            else if (jobtype == 3)
            { return 1; }
            else if (jobtype == 4)
            { return 0.4; }
            else
            { return 0.6; }
        }

        public double Marriage_Multiplier(bool marry,bool workpartner)
        {
            double marriage_multiplier=0;
            if (marry == true && workpartner == true)
            {
                return marriage_multiplier;
            }
            else if (marry == true && workpartner == false)
            {
                return marriage_multiplier + 0.2;
            }
            else
            { return marriage_multiplier; }
        }//TAMAMLANDI

        public double Children_Multiplier(bool Child_edu,int Child_age)//TAMAMLANDI
        {
            double children_multiplier=0;
            if(Child_age > 0)
            { 
                if (Child_age > 18 && Child_edu == true)
                { children_multiplier += 0.4; }
                else if (Child_age > 18 && Child_edu == false)
                { children_multiplier += 0; }
                else if (Child_age < 19 && Child_age > 6)
                { children_multiplier += 0.3; }
                else
                { children_multiplier += 0.2; }
                return children_multiplier;
            }
            else
            {
                return 0;
            }
        }

        public double City_Multiplier(int city)
        {
            if (city == 39)
            {
                return 0.15;
            }
            else if (city == 6 || city == 40)
            {
                return 0.1;
            }
            else if (city == 18 || city == 26 || city == 51 || city == 65 || city == 78 || city == 48 || city == 72)
            {
                return 0.05;
            }
            else if (city == 74 || city == 62 || city == 33 || city == 64 || city == 9 || city == 34 || city == 20 || city == 31 || city == 15 || city == 10 || city == 24)
            {
                return 0.03;
            }
            else if( city == 58 || city == 0 || city == 57 || city == 11 || city == 21 || city == 7 || city == 38 || city == 19)
            {
                return 0.03;
            }
            else
            {
                return 0;
            }
        }//ESKİ HALİ KULLANILDI

        string[] cities = new string[81];
        public string cityfinder(int city)
        { 
            cities = new string[]
            {
                "Adana","Adıyaman","Afyonkarahisar","Ağrı","Aksaray","Amasya","Ankara",//6
                "Antalya","Ardahan","Artvin","Aydın","Balıkesir","Bartın","Batman","Bayburt",//14
                "Bilecik","Bingöl","Bitlis","Bolu","Burdur","Bursa","Çanakkale","Çankırı",//22
                "Çorum","Denizli","Diyarbakır","Düzce","Edirne","Elazığ","Erzincan","Erzurum",//30
                "Eskişehir","Gaziantep","Giresun","Gümüşhane","Hakkari","Hatay","Iğdır",//37
                "Isparta","İstanbul","İzmir","Kahramanmaraş","Karabük","Karaman","Kars",//44
                "Kastamonu","Kayseri","Kırıkkale","Kırklareli","Kırşehir","Kilis","Kocaeli","Konya","Kütahya",//53
                "Malatya","Manisa","Mardin","Mersin","Muğla","Muş","Nevşehir","Niğde","Ordu",//62
                "Osmaniye","Rize","Sakarya","Samsun","Siirt","Sinop","Sivas","Şanlıurfa","Şırnak",//71
                "Tekirdağ","Tokat","Trabzon","Tunceli","Uşak","Van","Yalova","Yozgat","Zonguldak"//80
            };
            return cities[city];
        }

        string[] education = new string[6];
        public string educationwriter(int level)
        {
            education = new string[]
            {
            "Meslek alanı ile ilgili lisans",
            "Meslek alanı ile ilgili yüksek lisans",
            "Meslek alanı ile ilgili doktora",
            "Meslek alanı ile ilgili doçentlik",
            "Meslek alanı ile ilgili olmayan yüksek lisans",
            "Meslek alanı ile ilgili olmayan doktora/doçentlik"
            };
            return education[level];
        }

        string[] jobtype = new string[5];
        public string jobwriter(int level)
        {
            jobtype = new string[]
            {
           "Takım Lideri/Grup Yöneticisi/Teknik Yönetici/Yazılım Mimarı",
            "Proje Yöneticisi",
            "Direktör/Projeler Yöneticisi ",
            "CTO/Genel Müdür",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem Personeli Sayısı <= 5)",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem Personeli Sayısı > 5)"
            };
            return jobtype[level];
        }

        public void Deleter(int index)//TAMAMLANDI
        {
            employeeList.RemoveAt(index);
        }

        public int agecalculator(string date)//DENENİYOR!!!!!!!!!!!!!!      BAŞARISIZ !!!!!
        {
            string tmpToday = DateTime.UtcNow.ToShortDateString();
            string[] today = tmpToday.Split('.');
            int tmpTodayD = Int32.Parse(today[0]);
            int tmpTodayM = Int32.Parse(today[1]);
            int tmpTodayY = Int32.Parse(today[2]);
            string[] birth = date.Split('.');
            if(birth[0] != " " && birth[1] != " ")
            { 
                tmpTodayD = tmpTodayD - Int32.Parse(birth[0]);
                tmpTodayM = tmpTodayM - Int32.Parse(birth[1]);
                tmpTodayY = tmpTodayY - Int32.Parse(birth[2]);
                if ((tmpTodayM > 0) || (tmpTodayM == 0 && tmpTodayD >= 0))
                {
                    return tmpTodayY;
                }
                else
                {
                    return tmpTodayY - 1;
                }
            }
            else
            {
                return 0;
            }
        }

        public string listThemAll(string x)
        {
            string res = "";

            for (int i = 0; i < employeeList.Count; i++)
            {
                res += employeeList[i].identity.ToString() + x;
                res += employeeList[i].name + x;
                res += employeeList[i].surname + x;
                res += employeeList[i].localCity.ToString() + x;
                res += employeeList[i].workCity.ToString() + x;
                res += employeeList[i].educationalRank.ToString() + x;
                res += employeeList[i].additionalLanguages.ToString() + x;
                res = res.Remove(res.Length - 1);
                res += x;


                res += employeeList[i].jobType.ToString() + x;
                res += employeeList[i].marriage + x;
                res += employeeList[i].workingpartner.ToString() + x;
                res += employeeList[i].childrencount.ToString() + x;
                res += employeeList[i].Child1 + "/";
                res += employeeList[i].Child2 + "/";
                res = res.Remove(res.Length - 1);
                res += x;
                res += employeeList[i].Child1_Educated + "/";
                res += employeeList[i].Child2_Educated + "/";
                res = res.Remove(res.Length - 1);
                res += x;
                res += employeeList[i].salary.ToString() + Environment.NewLine;
            }
            return res;
        }



    }
}
