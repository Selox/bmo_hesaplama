using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeX
{
    public class Bilgi
    {
        public int identity { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int localCity { get; set; }
        public int workCity { get; set; }
        public int experince { get; set; }
        public bool English { get; set; }
        public int additionalLanguages { get; set; }
        public int educationalRank { get; set; }
        public int jobType { get; set; }
        public bool marriage { get; set; }
        public bool workingpartner { get; set; }
        public int childrencount { get; set; }
        public int[] childrenEducation { get; set; }
        public string Child1 { get; set; }
        public string Child2 { get; set; }
        public double salary { get; set; }
        public bool Child1_Educated { get; set; }
        public bool Child2_Educated { get; set; }
        public double multiplier { get; set; }
    }
}
