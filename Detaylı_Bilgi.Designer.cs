namespace ProjeX
{
    partial class Detaylı_Bilgi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Detay_İsim = new System.Windows.Forms.Label();
            this.Label_Detay_Name = new System.Windows.Forms.Label();
            this.Label_Detay_Soyisim = new System.Windows.Forms.Label();
            this.Label_Detay_YasSehir = new System.Windows.Forms.Label();
            this.Label_Detay_CalSehir = new System.Windows.Forms.Label();
            this.Label_Detay_Sektör = new System.Windows.Forms.Label();
            this.Label_Detay_Experince = new System.Windows.Forms.Label();
            this.Label_Detay_İngilizce = new System.Windows.Forms.Label();
            this.Label_Detay_Resim = new System.Windows.Forms.PictureBox();
            this.Label_Detay_YabancıDil = new System.Windows.Forms.Label();
            this.Label_Detay_Surname = new System.Windows.Forms.Label();
            this.Label_Detay_LocCity = new System.Windows.Forms.Label();
            this.Label_Detay_WorkCity = new System.Windows.Forms.Label();
            this.Label_Detay_English = new System.Windows.Forms.Label();
            this.Label_Detay_Education = new System.Windows.Forms.Label();
            this.Label_Detay_Type = new System.Windows.Forms.Label();
            this.Label_Detay_Marriage = new System.Windows.Forms.Label();
            this.Label_Detay_AdditionalLanguage = new System.Windows.Forms.Label();
            this.Label_Detay_Cocuk_2 = new System.Windows.Forms.Label();
            this.Label_Detay_Cocuk_1 = new System.Windows.Forms.Label();
            this.Label_Detay_Evli = new System.Windows.Forms.Label();
            this.Label_Detay_Ogrenim = new System.Windows.Forms.Label();
            this.Label_Detay_Tip = new System.Windows.Forms.Label();
            this.Label_Detay_Child_2 = new System.Windows.Forms.Label();
            this.Label_Detay_Child_1 = new System.Windows.Forms.Label();
            this.Label_Detay_Maas = new System.Windows.Forms.Label();
            this.Label_Detay_Çocuklar = new System.Windows.Forms.Label();
            this.Label_Detay_Eşİş = new System.Windows.Forms.Label();
            this.Label_Detay_WorkPartner = new System.Windows.Forms.Label();
            this.Label_Detay_Children = new System.Windows.Forms.Label();
            this.Buton_Kapat = new System.Windows.Forms.Button();
            this.Label_Detay_Salary = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Detay_Resim)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_Detay_İsim
            // 
            this.Label_Detay_İsim.AutoSize = true;
            this.Label_Detay_İsim.Location = new System.Drawing.Point(181, 13);
            this.Label_Detay_İsim.Name = "Label_Detay_İsim";
            this.Label_Detay_İsim.Size = new System.Drawing.Size(25, 13);
            this.Label_Detay_İsim.TabIndex = 0;
            this.Label_Detay_İsim.Text = "İsim";
            // 
            // Label_Detay_Name
            // 
            this.Label_Detay_Name.AutoSize = true;
            this.Label_Detay_Name.Location = new System.Drawing.Point(294, 13);
            this.Label_Detay_Name.Name = "Label_Detay_Name";
            this.Label_Detay_Name.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Name.TabIndex = 1;
            // 
            // Label_Detay_Soyisim
            // 
            this.Label_Detay_Soyisim.AutoSize = true;
            this.Label_Detay_Soyisim.Location = new System.Drawing.Point(181, 35);
            this.Label_Detay_Soyisim.Name = "Label_Detay_Soyisim";
            this.Label_Detay_Soyisim.Size = new System.Drawing.Size(42, 13);
            this.Label_Detay_Soyisim.TabIndex = 2;
            this.Label_Detay_Soyisim.Text = "Soyisim";
            // 
            // Label_Detay_YasSehir
            // 
            this.Label_Detay_YasSehir.AutoSize = true;
            this.Label_Detay_YasSehir.Location = new System.Drawing.Point(181, 58);
            this.Label_Detay_YasSehir.Name = "Label_Detay_YasSehir";
            this.Label_Detay_YasSehir.Size = new System.Drawing.Size(74, 13);
            this.Label_Detay_YasSehir.TabIndex = 3;
            this.Label_Detay_YasSehir.Text = "Yaşadığı Şehir";
            // 
            // Label_Detay_CalSehir
            // 
            this.Label_Detay_CalSehir.AutoSize = true;
            this.Label_Detay_CalSehir.Location = new System.Drawing.Point(181, 81);
            this.Label_Detay_CalSehir.Name = "Label_Detay_CalSehir";
            this.Label_Detay_CalSehir.Size = new System.Drawing.Size(69, 13);
            this.Label_Detay_CalSehir.TabIndex = 4;
            this.Label_Detay_CalSehir.Text = "Çalıştığı Şehir";
            // 
            // Label_Detay_Sektör
            // 
            this.Label_Detay_Sektör.AutoSize = true;
            this.Label_Detay_Sektör.Location = new System.Drawing.Point(181, 104);
            this.Label_Detay_Sektör.Name = "Label_Detay_Sektör";
            this.Label_Detay_Sektör.Size = new System.Drawing.Size(88, 13);
            this.Label_Detay_Sektör.TabIndex = 5;
            this.Label_Detay_Sektör.Text = "Sektör Tecrübesi";
            // 
            // Label_Detay_Experince
            // 
            this.Label_Detay_Experince.AutoSize = true;
            this.Label_Detay_Experince.Location = new System.Drawing.Point(294, 104);
            this.Label_Detay_Experince.Name = "Label_Detay_Experince";
            this.Label_Detay_Experince.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Experince.TabIndex = 6;
            // 
            // Label_Detay_İngilizce
            // 
            this.Label_Detay_İngilizce.AutoSize = true;
            this.Label_Detay_İngilizce.Location = new System.Drawing.Point(181, 126);
            this.Label_Detay_İngilizce.Name = "Label_Detay_İngilizce";
            this.Label_Detay_İngilizce.Size = new System.Drawing.Size(45, 13);
            this.Label_Detay_İngilizce.TabIndex = 7;
            this.Label_Detay_İngilizce.Text = "İngilizce";
            // 
            // Label_Detay_Resim
            // 
            this.Label_Detay_Resim.Location = new System.Drawing.Point(13, 13);
            this.Label_Detay_Resim.Name = "Label_Detay_Resim";
            this.Label_Detay_Resim.Size = new System.Drawing.Size(162, 163);
            this.Label_Detay_Resim.TabIndex = 8;
            this.Label_Detay_Resim.TabStop = false;
            // 
            // Label_Detay_YabancıDil
            // 
            this.Label_Detay_YabancıDil.AutoSize = true;
            this.Label_Detay_YabancıDil.Location = new System.Drawing.Point(181, 149);
            this.Label_Detay_YabancıDil.Name = "Label_Detay_YabancıDil";
            this.Label_Detay_YabancıDil.Size = new System.Drawing.Size(107, 13);
            this.Label_Detay_YabancıDil.TabIndex = 9;
            this.Label_Detay_YabancıDil.Text = "Ek Yabancı Dil Sayısı";
            // 
            // Label_Detay_Surname
            // 
            this.Label_Detay_Surname.AutoSize = true;
            this.Label_Detay_Surname.Location = new System.Drawing.Point(294, 35);
            this.Label_Detay_Surname.Name = "Label_Detay_Surname";
            this.Label_Detay_Surname.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Surname.TabIndex = 10;
            // 
            // Label_Detay_LocCity
            // 
            this.Label_Detay_LocCity.AutoSize = true;
            this.Label_Detay_LocCity.Location = new System.Drawing.Point(294, 58);
            this.Label_Detay_LocCity.Name = "Label_Detay_LocCity";
            this.Label_Detay_LocCity.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_LocCity.TabIndex = 11;
            // 
            // Label_Detay_WorkCity
            // 
            this.Label_Detay_WorkCity.AutoSize = true;
            this.Label_Detay_WorkCity.Location = new System.Drawing.Point(294, 81);
            this.Label_Detay_WorkCity.Name = "Label_Detay_WorkCity";
            this.Label_Detay_WorkCity.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_WorkCity.TabIndex = 12;
            // 
            // Label_Detay_English
            // 
            this.Label_Detay_English.AutoSize = true;
            this.Label_Detay_English.Location = new System.Drawing.Point(294, 126);
            this.Label_Detay_English.Name = "Label_Detay_English";
            this.Label_Detay_English.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_English.TabIndex = 13;
            // 
            // Label_Detay_Education
            // 
            this.Label_Detay_Education.AutoSize = true;
            this.Label_Detay_Education.Location = new System.Drawing.Point(505, 13);
            this.Label_Detay_Education.Name = "Label_Detay_Education";
            this.Label_Detay_Education.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Education.TabIndex = 14;
            // 
            // Label_Detay_Type
            // 
            this.Label_Detay_Type.AutoSize = true;
            this.Label_Detay_Type.Location = new System.Drawing.Point(505, 35);
            this.Label_Detay_Type.Name = "Label_Detay_Type";
            this.Label_Detay_Type.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Type.TabIndex = 15;
            // 
            // Label_Detay_Marriage
            // 
            this.Label_Detay_Marriage.AutoSize = true;
            this.Label_Detay_Marriage.Location = new System.Drawing.Point(505, 58);
            this.Label_Detay_Marriage.Name = "Label_Detay_Marriage";
            this.Label_Detay_Marriage.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Marriage.TabIndex = 16;
            // 
            // Label_Detay_AdditionalLanguage
            // 
            this.Label_Detay_AdditionalLanguage.AutoSize = true;
            this.Label_Detay_AdditionalLanguage.Location = new System.Drawing.Point(294, 149);
            this.Label_Detay_AdditionalLanguage.Name = "Label_Detay_AdditionalLanguage";
            this.Label_Detay_AdditionalLanguage.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_AdditionalLanguage.TabIndex = 17;
            // 
            // Label_Detay_Cocuk_2
            // 
            this.Label_Detay_Cocuk_2.AutoSize = true;
            this.Label_Detay_Cocuk_2.Location = new System.Drawing.Point(366, 149);
            this.Label_Detay_Cocuk_2.Name = "Label_Detay_Cocuk_2";
            this.Label_Detay_Cocuk_2.Size = new System.Drawing.Size(96, 13);
            this.Label_Detay_Cocuk_2.TabIndex = 38;
            this.Label_Detay_Cocuk_2.Text = "En Büyük 2.Çocuk";
            this.Label_Detay_Cocuk_2.Visible = false;
            // 
            // Label_Detay_Cocuk_1
            // 
            this.Label_Detay_Cocuk_1.AutoSize = true;
            this.Label_Detay_Cocuk_1.Location = new System.Drawing.Point(367, 126);
            this.Label_Detay_Cocuk_1.Name = "Label_Detay_Cocuk_1";
            this.Label_Detay_Cocuk_1.Size = new System.Drawing.Size(95, 13);
            this.Label_Detay_Cocuk_1.TabIndex = 37;
            this.Label_Detay_Cocuk_1.Text = "En büyük 1.Çocuk";
            this.Label_Detay_Cocuk_1.Visible = false;
            // 
            // Label_Detay_Evli
            // 
            this.Label_Detay_Evli.AutoSize = true;
            this.Label_Detay_Evli.Location = new System.Drawing.Point(367, 58);
            this.Label_Detay_Evli.Name = "Label_Detay_Evli";
            this.Label_Detay_Evli.Size = new System.Drawing.Size(43, 13);
            this.Label_Detay_Evli.TabIndex = 36;
            this.Label_Detay_Evli.Text = "Evli mi?";
            // 
            // Label_Detay_Ogrenim
            // 
            this.Label_Detay_Ogrenim.AutoSize = true;
            this.Label_Detay_Ogrenim.Location = new System.Drawing.Point(367, 13);
            this.Label_Detay_Ogrenim.Name = "Label_Detay_Ogrenim";
            this.Label_Detay_Ogrenim.Size = new System.Drawing.Size(46, 13);
            this.Label_Detay_Ogrenim.TabIndex = 35;
            this.Label_Detay_Ogrenim.Text = "Öğrenim";
            // 
            // Label_Detay_Tip
            // 
            this.Label_Detay_Tip.AutoSize = true;
            this.Label_Detay_Tip.Location = new System.Drawing.Point(367, 35);
            this.Label_Detay_Tip.Name = "Label_Detay_Tip";
            this.Label_Detay_Tip.Size = new System.Drawing.Size(35, 13);
            this.Label_Detay_Tip.TabIndex = 34;
            this.Label_Detay_Tip.Text = "İş Tipi";
            // 
            // Label_Detay_Child_2
            // 
            this.Label_Detay_Child_2.AutoSize = true;
            this.Label_Detay_Child_2.Location = new System.Drawing.Point(505, 149);
            this.Label_Detay_Child_2.Name = "Label_Detay_Child_2";
            this.Label_Detay_Child_2.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Child_2.TabIndex = 39;
            this.Label_Detay_Child_2.Visible = false;
            // 
            // Label_Detay_Child_1
            // 
            this.Label_Detay_Child_1.AutoSize = true;
            this.Label_Detay_Child_1.Location = new System.Drawing.Point(505, 126);
            this.Label_Detay_Child_1.Name = "Label_Detay_Child_1";
            this.Label_Detay_Child_1.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Child_1.TabIndex = 40;
            this.Label_Detay_Child_1.Visible = false;
            // 
            // Label_Detay_Maas
            // 
            this.Label_Detay_Maas.AutoSize = true;
            this.Label_Detay_Maas.Location = new System.Drawing.Point(181, 175);
            this.Label_Detay_Maas.Name = "Label_Detay_Maas";
            this.Label_Detay_Maas.Size = new System.Drawing.Size(33, 13);
            this.Label_Detay_Maas.TabIndex = 41;
            this.Label_Detay_Maas.Text = "Maaş";
            // 
            // Label_Detay_Çocuklar
            // 
            this.Label_Detay_Çocuklar.AutoSize = true;
            this.Label_Detay_Çocuklar.Location = new System.Drawing.Point(367, 104);
            this.Label_Detay_Çocuklar.Name = "Label_Detay_Çocuklar";
            this.Label_Detay_Çocuklar.Size = new System.Drawing.Size(82, 13);
            this.Label_Detay_Çocuklar.TabIndex = 42;
            this.Label_Detay_Çocuklar.Text = "Çocuğu Var mı?";
            // 
            // Label_Detay_Eşİş
            // 
            this.Label_Detay_Eşİş.AutoSize = true;
            this.Label_Detay_Eşİş.Location = new System.Drawing.Point(367, 81);
            this.Label_Detay_Eşİş.Name = "Label_Detay_Eşİş";
            this.Label_Detay_Eşİş.Size = new System.Drawing.Size(85, 13);
            this.Label_Detay_Eşİş.TabIndex = 43;
            this.Label_Detay_Eşİş.Text = "Eşi Çalışıyor mu?";
            // 
            // Label_Detay_WorkPartner
            // 
            this.Label_Detay_WorkPartner.AutoSize = true;
            this.Label_Detay_WorkPartner.Location = new System.Drawing.Point(505, 81);
            this.Label_Detay_WorkPartner.Name = "Label_Detay_WorkPartner";
            this.Label_Detay_WorkPartner.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_WorkPartner.TabIndex = 44;
            // 
            // Label_Detay_Children
            // 
            this.Label_Detay_Children.AutoSize = true;
            this.Label_Detay_Children.Location = new System.Drawing.Point(505, 104);
            this.Label_Detay_Children.Name = "Label_Detay_Children";
            this.Label_Detay_Children.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Children.TabIndex = 45;
            // 
            // Buton_Kapat
            // 
            this.Buton_Kapat.Location = new System.Drawing.Point(13, 203);
            this.Buton_Kapat.Name = "Buton_Kapat";
            this.Buton_Kapat.Size = new System.Drawing.Size(75, 23);
            this.Buton_Kapat.TabIndex = 46;
            this.Buton_Kapat.Text = "Kapat";
            this.Buton_Kapat.UseVisualStyleBackColor = true;
            this.Buton_Kapat.Click += new System.EventHandler(this.Buton_Kapat_Click);
            // 
            // Label_Detay_Salary
            // 
            this.Label_Detay_Salary.AutoSize = true;
            this.Label_Detay_Salary.Location = new System.Drawing.Point(246, 175);
            this.Label_Detay_Salary.Name = "Label_Detay_Salary";
            this.Label_Detay_Salary.Size = new System.Drawing.Size(0, 13);
            this.Label_Detay_Salary.TabIndex = 47;
            // 
            // Detaylı_Bilgi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 258);
            this.Controls.Add(this.Label_Detay_Salary);
            this.Controls.Add(this.Buton_Kapat);
            this.Controls.Add(this.Label_Detay_Children);
            this.Controls.Add(this.Label_Detay_WorkPartner);
            this.Controls.Add(this.Label_Detay_Eşİş);
            this.Controls.Add(this.Label_Detay_Çocuklar);
            this.Controls.Add(this.Label_Detay_Maas);
            this.Controls.Add(this.Label_Detay_Child_1);
            this.Controls.Add(this.Label_Detay_Child_2);
            this.Controls.Add(this.Label_Detay_Cocuk_2);
            this.Controls.Add(this.Label_Detay_Cocuk_1);
            this.Controls.Add(this.Label_Detay_Evli);
            this.Controls.Add(this.Label_Detay_Ogrenim);
            this.Controls.Add(this.Label_Detay_Tip);
            this.Controls.Add(this.Label_Detay_AdditionalLanguage);
            this.Controls.Add(this.Label_Detay_Marriage);
            this.Controls.Add(this.Label_Detay_Type);
            this.Controls.Add(this.Label_Detay_Education);
            this.Controls.Add(this.Label_Detay_English);
            this.Controls.Add(this.Label_Detay_WorkCity);
            this.Controls.Add(this.Label_Detay_LocCity);
            this.Controls.Add(this.Label_Detay_Surname);
            this.Controls.Add(this.Label_Detay_YabancıDil);
            this.Controls.Add(this.Label_Detay_Resim);
            this.Controls.Add(this.Label_Detay_İngilizce);
            this.Controls.Add(this.Label_Detay_Experince);
            this.Controls.Add(this.Label_Detay_Sektör);
            this.Controls.Add(this.Label_Detay_CalSehir);
            this.Controls.Add(this.Label_Detay_YasSehir);
            this.Controls.Add(this.Label_Detay_Soyisim);
            this.Controls.Add(this.Label_Detay_Name);
            this.Controls.Add(this.Label_Detay_İsim);
            this.Name = "Detaylı_Bilgi";
            this.Text = "Detaylı_Bilgi";
            this.Load += new System.EventHandler(this.Detaylı_Bilgi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Label_Detay_Resim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Detay_İsim;
        private System.Windows.Forms.Label Label_Detay_Name;
        private System.Windows.Forms.Label Label_Detay_Soyisim;
        private System.Windows.Forms.Label Label_Detay_YasSehir;
        private System.Windows.Forms.Label Label_Detay_CalSehir;
        private System.Windows.Forms.Label Label_Detay_Sektör;
        private System.Windows.Forms.Label Label_Detay_Experince;
        private System.Windows.Forms.Label Label_Detay_İngilizce;
        private System.Windows.Forms.PictureBox Label_Detay_Resim;
        private System.Windows.Forms.Label Label_Detay_YabancıDil;
        private System.Windows.Forms.Label Label_Detay_Surname;
        private System.Windows.Forms.Label Label_Detay_LocCity;
        private System.Windows.Forms.Label Label_Detay_WorkCity;
        private System.Windows.Forms.Label Label_Detay_English;
        private System.Windows.Forms.Label Label_Detay_Education;
        private System.Windows.Forms.Label Label_Detay_Type;
        private System.Windows.Forms.Label Label_Detay_Marriage;
        private System.Windows.Forms.Label Label_Detay_AdditionalLanguage;
        private System.Windows.Forms.Label Label_Detay_Cocuk_2;
        private System.Windows.Forms.Label Label_Detay_Cocuk_1;
        private System.Windows.Forms.Label Label_Detay_Evli;
        private System.Windows.Forms.Label Label_Detay_Ogrenim;
        private System.Windows.Forms.Label Label_Detay_Tip;
        private System.Windows.Forms.Label Label_Detay_Child_2;
        private System.Windows.Forms.Label Label_Detay_Child_1;
        private System.Windows.Forms.Label Label_Detay_Maas;
        private System.Windows.Forms.Label Label_Detay_Çocuklar;
        private System.Windows.Forms.Label Label_Detay_Eşİş;
        private System.Windows.Forms.Label Label_Detay_WorkPartner;
        private System.Windows.Forms.Label Label_Detay_Children;
        private System.Windows.Forms.Button Buton_Kapat;
        private System.Windows.Forms.Label Label_Detay_Salary;
    }
}