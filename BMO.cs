using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProjeX
{
    public partial class BMO : Form
    {
        Employee eleman = new Employee();
        List<Bilgi> yazma = new List<Bilgi>();
        int id=0;
        public BMO()
        {
            InitializeComponent();
        }

        public void Button_Ekle_Click(object sender, EventArgs e)
        {
            id++;
            string İsim = Text_İsim.Text;
            string Soyisim = Text_Soy.Text;
            int YasSehir = Box_YasSehir.SelectedIndex;
            int CalSehir = Box_CalSehir.SelectedIndex;
            int Deneyim = 0;
            if (Text_Tecrübe.Text == " ")
            {
                Deneyim = 0;
            }
            else
            {
                Deneyim = Int32.Parse(Text_Tecrübe.Text);
            }
            bool İngilizce = false;
            if (İngilizce_Var.Checked == true)
            {
                 İngilizce = true;
            }
            else
            {
                 İngilizce = false;
            }
            int ekdiller = 0;
            if (Text_EkDil.Text == "")
            {
                 ekdiller = 0;
            }
            else
            {
                 ekdiller = Int32.Parse(Text_EkDil.Text);
            }
            int Ögrenim = 0;
            if (Box_Ogrenim.SelectedItem.ToString() != " ")
            {
                 Ögrenim = Box_Ogrenim.SelectedIndex;
            }
            else
            {
                 Ögrenim = 0;
            }
            int İstipi = 0;
            if (Box_Tip.SelectedItem.ToString() != " ")
            {
                İstipi = Box_Tip.SelectedIndex;
            }
            else
            {
                 İstipi = 0;
            }
            bool Evlilik, Eşin_Çalışması;
            int cocuksayisi;
            cocuksayisi = Int32.Parse(Box_Cocuk.Text);
            if (Check_Evli_Evet.Checked)
            {
                Evlilik = true;
                if (Check_Eşi_Çalışıyor.Checked)
                {
                    Eşin_Çalışması = true;
                }
                else
                {
                    Eşin_Çalışması = false;
                }
            }
            else
            {
                Evlilik = false;
                Eşin_Çalışması = false;
            }
            string Tarih1="",Tarih2="";
            if(cocuksayisi >= 2)
            {
                Tarih1 += Text_Cocuk1_Dogum_Gün.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Ay.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Yıl.Text + ".";
            
                Tarih2 += Text_Cocuk2_Dogum_Gün.Text + ".";
                Tarih2 += Text_Cocuk2_Dogum_Ay.Text + ".";
                Tarih2 += Text_Cocuk2_Dogum_Yıl.Text + ".";
            }
            else if(cocuksayisi == 1)
            {
                Tarih1 += Text_Cocuk1_Dogum_Gün.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Ay.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Yıl.Text + ".";
                Tarih2 = " " + "." + " " + "." + " " + ".";
            }
            else
            {
                Tarih1 = " " + "." + " " + "." + " " + ".";
                Tarih2 = " " + "." + " " + "." + " " + ".";
            }
            bool Cocuk1_Okuyor, Cocuk2_Okuyor;
            if(true == Box_Cocuk1_Eğitim.Checked)
            {
                Cocuk1_Okuyor = true;
            }
            else
            {
                Cocuk1_Okuyor = false;
            }

            if (true == Box_Cocuk2_Eğitim.Checked)
            {
                Cocuk2_Okuyor = true;
            }
            else
            {
                Cocuk2_Okuyor = false;
            }
            eleman.Staff_add(İsim, Soyisim, YasSehir, CalSehir, Deneyim, İngilizce, ekdiller, Ögrenim , İstipi ,Evlilik 
               , Eşin_Çalışması, cocuksayisi, Tarih1, Tarih2, Cocuk1_Okuyor,Cocuk2_Okuyor);
            string yazı = id.ToString() + " " + İsim + " " + Soyisim + " " + YasSehir;
            Yazdirici(id);
        } //PROTOTİP TAMAM EKLEMELER YAPILACAK

        public void Yazdirici(int ide)
        {
            string yazı = eleman.Printer(ide);
            List_Calisanlar.Items.Add(eleman.Printer(ide) + "\n");
            //List_Calisanlar.Items.Add(yazı + "\n");
        }//PROTOTİP TAMAM EKLEMELER YAPILACAK

        /* public void deleteStaffFromList(int indeksus)
          {
              int index = -1;
              for (int i = 0; i < List_Calisanlar.Items.Count; i++)
              {
                  ListBox item = List_Calisanlar.Items[i];
                  if (item.SubItems[0].Text == indeksus.ToString())
                  {
                      index = i;
                  }
              }
              if (index != -1)
              {
                  List_Calisanlar.Items.RemoveAt(index);
              }
          }*/

        public void Buton_Sil_Click(object sender, EventArgs e)//HATALI KOD!!!!
        {
            int indeks = List_Calisanlar.SelectedIndex;
            eleman.Deleter(indeks);
            List_Calisanlar.Items.RemoveAt(indeks);
        }

        public void Buton_Güncelle_Click(object sender, EventArgs e)//EKSİKLER VAR
        {
            int index = List_Calisanlar.SelectedIndex;
            string İsim = Text_İsim.Text;
            string Soyisim = Text_Soy.Text;
            int YasSehir = Box_YasSehir.SelectedIndex;
            int CalSehir = Box_CalSehir.SelectedIndex;
            int Deneyim = 0;
            if (Text_Tecrübe.Text == " ")
            {
                Deneyim = 0;
            }
            else
            {
                Deneyim = Int32.Parse(Text_Tecrübe.Text);
            }
            bool İngilizce = false;
            if (İngilizce_Var.Checked == true)
            {
                İngilizce = true;
            }
            else
            {
                İngilizce = false;
            }
            int ekdiller = 0;
            if (Text_EkDil.Text == "")
            {
                ekdiller = 0;
            }
            else
            {
                ekdiller = Int32.Parse(Text_EkDil.Text);
            }
            int Ögrenim = 0;
            if (Box_Ogrenim.SelectedItem.ToString() != " ")
            {
                Ögrenim = Box_Ogrenim.SelectedIndex;
            }
            else
            {
                Ögrenim = 0;
            }
            int İstipi = 0;
            if (Box_Tip.SelectedItem.ToString() != " ")
            {
                İstipi = Box_Tip.SelectedIndex;
            }
            else
            {
                İstipi = 0;
            }
            bool Evlilik, Eşin_Çalışması;
            int cocuksayisi;
            cocuksayisi = Int32.Parse(Box_Cocuk.Text);
            if (Check_Evli_Evet.Checked)
            {
                Evlilik = true;
                if (Check_Eşi_Çalışıyor.Checked)
                {
                    Eşin_Çalışması = true;
                }
                else
                {
                    Eşin_Çalışması = false;
                }
            }
            else
            {
                Evlilik = false;
                Eşin_Çalışması = false;
            }
            string Tarih1 = "", Tarih2 = "";
            if (cocuksayisi >= 2)
            {
                Tarih1 += Text_Cocuk1_Dogum_Gün.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Ay.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Yıl.Text + ".";

                Tarih2 += Text_Cocuk2_Dogum_Gün.Text + ".";
                Tarih2 += Text_Cocuk2_Dogum_Ay.Text + ".";
                Tarih2 += Text_Cocuk2_Dogum_Yıl.Text + ".";
            }
            else if (cocuksayisi == 1)
            {
                Tarih1 += Text_Cocuk1_Dogum_Gün.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Ay.Text + ".";
                Tarih1 += Text_Cocuk1_Dogum_Yıl.Text + ".";
                Tarih2 = " " + "." + " " + "." + " " + ".";
            }
            else
            {
                Tarih1 = " " + "." + " " + "." + " " + ".";
                Tarih2 = " " + "." + " " + "." + " " + ".";
            }
            bool Cocuk1_Okuyor, Cocuk2_Okuyor;
            if (true == Box_Cocuk1_Eğitim.Checked)
            {
                Cocuk1_Okuyor = true;
            }
            else
            {
                Cocuk1_Okuyor = false;
            }

            if (true == Box_Cocuk2_Eğitim.Checked)
            {
                Cocuk2_Okuyor = true;
            }
            else
            {
                Cocuk2_Okuyor = false;
            }
            eleman.Staff_update(index,İsim, Soyisim, YasSehir, CalSehir, Deneyim, İngilizce, ekdiller, Ögrenim, İstipi, Evlilik
                , Eşin_Çalışması, cocuksayisi, Tarih1, Tarih2, Cocuk1_Okuyor, Cocuk2_Okuyor);
            string yazı = id + " " + İsim + " " + Soyisim + " " + YasSehir;
            Yazdirici(id);
        }

        public void Check_Evli_Evet_CheckedChanged(object sender, EventArgs e)//TAMAMLANDI
        {
            if (Check_Evli_Evet.Checked == true)
            {
                Label_Cocuk.Visible = true;
                Box_Cocuk.Visible = true;
                Check_Eşi_Çalışıyor.Visible = true;
                Check_Eşi_Çalışıyor.Enabled = true;
            }
            else
            {
                Check_Eşi_Çalışıyor.Enabled = false;
                Check_Eşi_Çalışıyor.Visible = false;
                Label_Cocuk.Visible = true;
                Box_Cocuk.Visible = true;
            }
        }

        private void Buton_Kaydet_Click(object sender, EventArgs e)
         {
             string kaydedilecek = String.Empty;
            int i = 0;
             for (i = 0; i < List_Calisanlar.Items.Count; i++)
             {
                 kaydedilecek += List_Calisanlar.Items[i].ToString() + Environment.NewLine;
             }

             Dosya_Kaydet.Filter = "Text Dosyası(*.csv; *.tsv) | *.csv; *.tsv";
            if (Dosya_Kaydet.ShowDialog() == DialogResult.OK)
            {
                if (Dosya_Kaydet.FilterIndex == 1)
                {
                    kaydedilecek = eleman.listThemAll(",");
                }
                else if (Dosya_Kaydet.FilterIndex == 2)
                {
                    kaydedilecek = eleman.listThemAll("\t");
                }

                StreamWriter streamWriter = new StreamWriter(Dosya_Kaydet.FileName);
                streamWriter.WriteLine(kaydedilecek);
                streamWriter.Close();
            }

            for (i=0 ; i < 100; i++)
            {
                BMO_Progress.Value++;
            }

        }
         
        public void Buton_Yükle_Click(object sender, EventArgs e)
        {
            string Path = "";
            char harf=',';
            int i =0;
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Dosya Açma";
            open.Filter = "Text Dosyası(*.csv; *.tsv) | *.csv; *.tsv";
            open.InitialDirectory = @"d:\";
            if (Dosya_Kaydet.ShowDialog() == DialogResult.OK)
            {
                if (Dosya_Kaydet.FilterIndex == 1)
                {
                    harf = ',';
                }
                else if (Dosya_Kaydet.FilterIndex == 2)
                {
                    harf = '\t';
                }
                else if (Dosya_Kaydet.FilterIndex == 3)
                {
                    int j = 0;
                    List_Calisanlar.Items.Clear();

                    StreamReader streamReader = new StreamReader(Dosya_Kaydet.FileName);
                    Path += streamReader.ReadToEnd();
                streamReader.Close();

                List_Calisanlar.Items.Clear();

                    Path = Path.Trim();

                if (Path != "")
                {
                    string[] lines = Path.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(harf);

                        int id = Int32.Parse(elements[0]);
                        string name = elements[1];
                        string surname = elements[2];
                        int LocalCity = Int32.Parse(elements[3]);
                        int workCity = Int32.Parse(elements[4]);
                        int experience = Int32.Parse(elements[5]);
                        bool İngiliz = bool.Parse(elements[6]);
                        int additionalLanguage = Int32.Parse(elements[7]);

                        int educationRank = Int32.Parse(elements[8]);
                        

                        int jobtype = Int32.Parse(elements[9]);
                        bool marriage = false;
                        if (elements[10] != "False")
                        {
                            marriage = true;
                        }

                        bool workingpartner = bool.Parse(elements[11]);
                        int childrencount = Int32.Parse(elements[12]);

                        string Child1 = elements[13];
                        string Child2 = elements[14];

                        bool Child1_Edu = bool.Parse(elements[15]);
                        bool Child2_Edu = bool.Parse(elements[16]);

                        double salary = Double.Parse(elements[17]);

                        eleman.StaffID = id;

                        // Listview Staff Ekleme.
                        string[] staff = { id.ToString(), name, surname, salary.ToString() };
                        List_Calisanlar.Items.Add(staff);

                        // Databese Staff Ekleme.
                        eleman.Staff_add(name, surname, LocalCity, workCity, experience,İngiliz, additionalLanguage, educationRank
                                    , jobtype, marriage, workingpartner, childrencount,Child1,Child2,
                                    Child1_Edu,Child2_Edu);
                    }
                }
            }
            for (i=0; i < 100; i++)
            {
                BMO_Progress.Value++;
            }
        }
    }

        void List_Calisanlar_MouseDoubleClick(object sender, MouseEventArgs e)

        {
            int index = this.List_Calisanlar.IndexFromPoint(e.Location);

            if (index != System.Windows.Forms.ListBox.NoMatches)

            {

               /* MessageBox.Show(index.ToString());
                string[] arr = { elemanListe[0].name, elemanListe[0].surname };
                //Detaylı_Bilgi Detaylı_Bilgi = new Detaylı_Bilgi(this, arr);
                Detaylı_Bilgi.Show();*/

            }

        }//GÜNCELLENECEK

        public void Box_Cocuk_TextChanged(object sender, EventArgs e)
        {

            if(Box_Cocuk.Text == "")
            {

            }
            else if((Box_Cocuk.Text == "-" || Box_Cocuk.Text == "+") || Box_Cocuk.Text == " ")
            {
                Box_Cocuk.Clear();
            }
            else
            { 
            int cocuksayisi = Int32.Parse(Box_Cocuk.Text);
                if (cocuksayisi >= 2)
                {
                    Label_Cocuk1_Eğitim.Visible = true;
                    Label_Cocuk_1.Visible = true;
                    Label_Cocuk_2.Visible = true;
                    Text_Cocuk1_Dogum_Gün.Enabled = true;
                    Text_Cocuk1_Dogum_Gün.Visible = true;
                    Text_Cocuk1_Dogum_Ay.Enabled = true;
                    Text_Cocuk1_Dogum_Ay.Visible = true;
                    Text_Cocuk1_Dogum_Yıl.Enabled = true;
                    Text_Cocuk1_Dogum_Yıl.Visible = true;
                    Box_Cocuk1_Eğitim.Visible = true;
                    Label_Cocuk2_Eğitim.Visible = true;
                    Text_Cocuk2_Dogum_Gün.Enabled = true;
                    Text_Cocuk2_Dogum_Gün.Visible = true;
                    Text_Cocuk2_Dogum_Ay.Enabled = true;
                    Text_Cocuk2_Dogum_Ay.Visible = true;
                    Text_Cocuk2_Dogum_Yıl.Enabled = true;
                    Text_Cocuk2_Dogum_Yıl.Visible = true;
                    Box_Cocuk2_Eğitim.Visible = true;
                }
                else if (cocuksayisi == 1)
                {
                    Label_Cocuk_1.Visible = true;
                    Text_Cocuk1_Dogum_Gün.Enabled = true;
                    Text_Cocuk1_Dogum_Gün.Visible = true;
                    Text_Cocuk1_Dogum_Ay.Enabled = true;
                    Text_Cocuk1_Dogum_Ay.Visible = true;
                    Text_Cocuk1_Dogum_Yıl.Enabled = true;
                    Text_Cocuk1_Dogum_Yıl.Visible = true;
                    Box_Cocuk1_Eğitim.Visible = true;
                    Label_Cocuk1_Eğitim.Visible = true;
                    Label_Cocuk2_Eğitim.Visible = false;
                    Label_Cocuk_2.Visible = false;
                    Text_Cocuk2_Dogum_Gün.Enabled = false;
                    Text_Cocuk2_Dogum_Gün.Visible = false;
                    Text_Cocuk2_Dogum_Ay.Enabled = false;
                    Text_Cocuk2_Dogum_Ay.Visible = false;
                    Text_Cocuk2_Dogum_Yıl.Enabled = false;
                    Text_Cocuk2_Dogum_Yıl.Visible = false;
                    Box_Cocuk2_Eğitim.Visible = false;
                }
                else
                {
                    Label_Cocuk_1.Visible = false;
                    Label_Cocuk_2.Visible = false;
                    Text_Cocuk1_Dogum_Gün.Enabled = false;
                    Text_Cocuk1_Dogum_Gün.Visible = false;
                    Text_Cocuk1_Dogum_Ay.Enabled = false;
                    Text_Cocuk1_Dogum_Ay.Visible = false;
                    Text_Cocuk1_Dogum_Yıl.Enabled = false;
                    Text_Cocuk1_Dogum_Yıl.Visible = false;
                    Box_Cocuk1_Eğitim.Visible = false;
                    Label_Cocuk1_Eğitim.Visible = false;
                    Label_Cocuk2_Eğitim.Visible = false;
                    Text_Cocuk2_Dogum_Gün.Enabled = false;
                    Text_Cocuk2_Dogum_Gün.Visible = false;
                    Text_Cocuk2_Dogum_Ay.Enabled = false;
                    Text_Cocuk2_Dogum_Ay.Visible = false;
                    Text_Cocuk2_Dogum_Yıl.Enabled = false;
                    Text_Cocuk2_Dogum_Yıl.Visible = false;
                    Box_Cocuk2_Eğitim.Visible = false;
                }
            }
        } //PROTOTİP TAMAMLANDI

        public void Buton_Bilgi_Click_1(object sender, EventArgs e)
        {
            int indeks = List_Calisanlar.SelectedIndex;
            Detaylı_Bilgi D = new Detaylı_Bilgi(indeks, this, eleman.degisken());
            D.Show();
        }//PROTOTİP TAMAMLANDI

        private void Buton_Resim_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Dosya Açma";
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            open.InitialDirectory = @"d:\";
            if (open.ShowDialog() == DialogResult.OK)
            {
                Text_Resim.Text = open.FileName;
                MessageBox.Show(Text_Resim.Text);
                Box_Resim.Image = new Bitmap(open.FileName);

            }
        }

        private void BMO_FormKapatma(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = dialog = MessageBox.Show("Programı gerçekten kapatmak istediğine emin misin?", "Are You Ciddi?", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }


    }
}
