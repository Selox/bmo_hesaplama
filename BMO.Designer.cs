namespace ProjeX
{
    partial class BMO
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BMO));
            this.Text_İsim = new System.Windows.Forms.TextBox();
            this.Label_İsim = new System.Windows.Forms.Label();
            this.Button_Ekle = new System.Windows.Forms.Button();
            this.List_Calisanlar = new System.Windows.Forms.ListBox();
            this.Label_Soyisim = new System.Windows.Forms.Label();
            this.Text_Soy = new System.Windows.Forms.TextBox();
            this.Box_YasSehir = new System.Windows.Forms.ComboBox();
            this.Label_YasSehir = new System.Windows.Forms.Label();
            this.Label_CalSehir = new System.Windows.Forms.Label();
            this.Box_CalSehir = new System.Windows.Forms.ComboBox();
            this.Label_Tecrübe = new System.Windows.Forms.Label();
            this.Text_Tecrübe = new System.Windows.Forms.TextBox();
            this.Buton_Sil = new System.Windows.Forms.Button();
            this.Buton_Güncelle = new System.Windows.Forms.Button();
            this.Label_İngilizce = new System.Windows.Forms.Label();
            this.İngilizce_Var = new System.Windows.Forms.CheckBox();
            this.Label_EkYabancıDil = new System.Windows.Forms.Label();
            this.Text_EkDil = new System.Windows.Forms.TextBox();
            this.Label_Tip = new System.Windows.Forms.Label();
            this.Box_Tip = new System.Windows.Forms.ComboBox();
            this.Box_Ogrenim = new System.Windows.Forms.ComboBox();
            this.Label_Ogrenim = new System.Windows.Forms.Label();
            this.Label_Evli = new System.Windows.Forms.Label();
            this.Check_Evli_Evet = new System.Windows.Forms.CheckBox();
            this.Label_Cocuk = new System.Windows.Forms.Label();
            this.Label_Cocuk_2 = new System.Windows.Forms.Label();
            this.Label_Cocuk_1 = new System.Windows.Forms.Label();
            this.Check_Eşi_Çalışıyor = new System.Windows.Forms.CheckBox();
            this.Buton_Kaydet = new System.Windows.Forms.Button();
            this.Buton_Yükle = new System.Windows.Forms.Button();
            this.Dosya_Kaydet = new System.Windows.Forms.SaveFileDialog();
            this.Box_Resim = new System.Windows.Forms.PictureBox();
            this.Label_Picture = new System.Windows.Forms.Label();
            this.Text_Resim = new System.Windows.Forms.TextBox();
            this.Buton_Resim = new System.Windows.Forms.Button();
            this.Buton_Bilgi = new System.Windows.Forms.Button();
            this.Text_Cocuk1_Dogum_Gün = new System.Windows.Forms.TextBox();
            this.Text_Cocuk1_Dogum_Ay = new System.Windows.Forms.TextBox();
            this.Text_Cocuk1_Dogum_Yıl = new System.Windows.Forms.TextBox();
            this.Text_Cocuk2_Dogum_Yıl = new System.Windows.Forms.TextBox();
            this.Text_Cocuk2_Dogum_Ay = new System.Windows.Forms.TextBox();
            this.Text_Cocuk2_Dogum_Gün = new System.Windows.Forms.TextBox();
            this.Box_Cocuk1_Eğitim = new System.Windows.Forms.CheckBox();
            this.Label_Cocuk1_Eğitim = new System.Windows.Forms.Label();
            this.Box_Cocuk2_Eğitim = new System.Windows.Forms.CheckBox();
            this.Label_Cocuk2_Eğitim = new System.Windows.Forms.Label();
            this.Box_Cocuk = new System.Windows.Forms.TextBox();
            this.BMO_Progress = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.Box_Resim)).BeginInit();
            this.SuspendLayout();
            // 
            // Text_İsim
            // 
            this.Text_İsim.Location = new System.Drawing.Point(127, 8);
            this.Text_İsim.Name = "Text_İsim";
            this.Text_İsim.Size = new System.Drawing.Size(100, 20);
            this.Text_İsim.TabIndex = 0;
            // 
            // Label_İsim
            // 
            this.Label_İsim.AutoSize = true;
            this.Label_İsim.Location = new System.Drawing.Point(12, 15);
            this.Label_İsim.Name = "Label_İsim";
            this.Label_İsim.Size = new System.Drawing.Size(25, 13);
            this.Label_İsim.TabIndex = 1;
            this.Label_İsim.Text = "İsim";
            // 
            // Button_Ekle
            // 
            this.Button_Ekle.Location = new System.Drawing.Point(713, 5);
            this.Button_Ekle.Name = "Button_Ekle";
            this.Button_Ekle.Size = new System.Drawing.Size(75, 23);
            this.Button_Ekle.TabIndex = 2;
            this.Button_Ekle.Text = "Ekle";
            this.Button_Ekle.UseVisualStyleBackColor = true;
            this.Button_Ekle.Click += new System.EventHandler(this.Button_Ekle_Click);
            // 
            // List_Calisanlar
            // 
            this.List_Calisanlar.FormattingEnabled = true;
            this.List_Calisanlar.Location = new System.Drawing.Point(240, 214);
            this.List_Calisanlar.Name = "List_Calisanlar";
            this.List_Calisanlar.Size = new System.Drawing.Size(548, 225);
            this.List_Calisanlar.TabIndex = 4;
            // 
            // Label_Soyisim
            // 
            this.Label_Soyisim.AutoSize = true;
            this.Label_Soyisim.Location = new System.Drawing.Point(12, 40);
            this.Label_Soyisim.Name = "Label_Soyisim";
            this.Label_Soyisim.Size = new System.Drawing.Size(42, 13);
            this.Label_Soyisim.TabIndex = 6;
            this.Label_Soyisim.Text = "Soyisim";
            // 
            // Text_Soy
            // 
            this.Text_Soy.Location = new System.Drawing.Point(127, 35);
            this.Text_Soy.Name = "Text_Soy";
            this.Text_Soy.Size = new System.Drawing.Size(100, 20);
            this.Text_Soy.TabIndex = 5;
            // 
            // Box_YasSehir
            // 
            this.Box_YasSehir.FormattingEnabled = true;
            this.Box_YasSehir.Items.AddRange(new object[] {
            "Adana",
            "Adıyaman",
            "Afyonkarahisar",
            "Ağrı",
            "Aksaray",
            "Amasya",
            "Ankara",
            "Antalya",
            "Ardahan",
            "Artvin",
            "Aydın",
            "Balıkesir",
            "Bartın",
            "Batman",
            "Bayburt",
            "Bilecik",
            "Bingöl",
            "Bitlis",
            "Bolu",
            "Burdur",
            "Bursa",
            "Çanakkale",
            "Çankırı",
            "Çorum",
            "Denzli",
            "Diyarbakır",
            "Düzce",
            "Edirne",
            "Elazığ",
            "Erzincan",
            "Erzurum",
            "Eskişehir",
            "Gaziantep",
            "Giresun",
            "Gümüşhane",
            "Hakkari",
            "Hatay",
            "Iğdır",
            "Isparta",
            "İstanbul",
            "İzmir",
            "Kahramanmaraş",
            "Karabük",
            "Karaman",
            "Kars",
            "Kastamonu",
            "Kayseri",
            "Kırıkkale",
            "Kırklareli",
            "Kırşehir",
            "Kilis",
            "Kocaeli",
            "Konya",
            "Kütahya",
            "Malatya",
            "Manisa",
            "Mardin",
            "Mersin",
            "Muğla",
            "Muş",
            "Nevşehir",
            "Niğde",
            "Ordu",
            "Osmaniye",
            "Rize",
            "Sakarya",
            "Samsun",
            "Siirt",
            "Sinop",
            "Sivas",
            "Şanlıurfa",
            "Şırnak",
            "Tekirdağ",
            "Tokat",
            "Trabzon",
            "Tunceli",
            "Uşak",
            "Van",
            "Yalova",
            "Yozgat",
            "Zonguldak"});
            this.Box_YasSehir.Location = new System.Drawing.Point(127, 63);
            this.Box_YasSehir.Name = "Box_YasSehir";
            this.Box_YasSehir.Size = new System.Drawing.Size(100, 21);
            this.Box_YasSehir.TabIndex = 7;
            // 
            // Label_YasSehir
            // 
            this.Label_YasSehir.AutoSize = true;
            this.Label_YasSehir.Location = new System.Drawing.Point(12, 66);
            this.Label_YasSehir.Name = "Label_YasSehir";
            this.Label_YasSehir.Size = new System.Drawing.Size(80, 13);
            this.Label_YasSehir.TabIndex = 8;
            this.Label_YasSehir.Text = "Yaşanılan Şehir";
            // 
            // Label_CalSehir
            // 
            this.Label_CalSehir.AutoSize = true;
            this.Label_CalSehir.Location = new System.Drawing.Point(12, 94);
            this.Label_CalSehir.Name = "Label_CalSehir";
            this.Label_CalSehir.Size = new System.Drawing.Size(72, 13);
            this.Label_CalSehir.TabIndex = 10;
            this.Label_CalSehir.Text = "Çalışılan Şehir";
            // 
            // Box_CalSehir
            // 
            this.Box_CalSehir.FormattingEnabled = true;
            this.Box_CalSehir.Items.AddRange(new object[] {
            "Adana",
            "Adıyaman",
            "Afyonkarahisar",
            "Ağrı",
            "Aksaray",
            "Amasya",
            "Ankara",
            "Antalya",
            "Ardahan",
            "Artvin",
            "Aydın",
            "Balıkesir",
            "Bartın",
            "Batman",
            "Bayburt",
            "Bilecik",
            "Bingöl",
            "Bitlis",
            "Bolu",
            "Burdur",
            "Bursa",
            "Çanakkale",
            "Çankırı",
            "Çorum",
            "Denzli",
            "Diyarbakır",
            "Düzce",
            "Edirne",
            "Elazığ",
            "Erzincan",
            "Erzurum",
            "Eskişehir",
            "Gaziantep",
            "Giresun",
            "Gümüşhane",
            "Hakkari",
            "Hatay",
            "Iğdır",
            "Isparta",
            "İstanbul",
            "İzmir",
            "Kahramanmaraş",
            "Karabük",
            "Karaman",
            "Kars",
            "Kastamonu",
            "Kayseri",
            "Kırıkkale",
            "Kırklareli",
            "Kırşehir",
            "Kilis",
            "Kocaeli",
            "Konya",
            "Kütahya",
            "Malatya",
            "Manisa",
            "Mardin",
            "Mersin",
            "Muğla",
            "Muş",
            "Nevşehir",
            "Niğde",
            "Ordu",
            "Osmaniye",
            "Rize",
            "Sakarya",
            "Samsun",
            "Siirt",
            "Sinop",
            "Sivas",
            "Şanlıurfa",
            "Şırnak",
            "Tekirdağ",
            "Tokat",
            "Trabzon",
            "Tunceli",
            "Uşak",
            "Van",
            "Yalova",
            "Yozgat",
            "Zonguldak"});
            this.Box_CalSehir.Location = new System.Drawing.Point(127, 91);
            this.Box_CalSehir.Name = "Box_CalSehir";
            this.Box_CalSehir.Size = new System.Drawing.Size(100, 21);
            this.Box_CalSehir.TabIndex = 9;
            // 
            // Label_Tecrübe
            // 
            this.Label_Tecrübe.AutoSize = true;
            this.Label_Tecrübe.Location = new System.Drawing.Point(12, 119);
            this.Label_Tecrübe.Name = "Label_Tecrübe";
            this.Label_Tecrübe.Size = new System.Drawing.Size(88, 13);
            this.Label_Tecrübe.TabIndex = 12;
            this.Label_Tecrübe.Text = "Sektör Tecrübesi";
            // 
            // Text_Tecrübe
            // 
            this.Text_Tecrübe.Location = new System.Drawing.Point(182, 119);
            this.Text_Tecrübe.Name = "Text_Tecrübe";
            this.Text_Tecrübe.Size = new System.Drawing.Size(45, 20);
            this.Text_Tecrübe.TabIndex = 11;
            // 
            // Buton_Sil
            // 
            this.Buton_Sil.Location = new System.Drawing.Point(713, 37);
            this.Buton_Sil.Name = "Buton_Sil";
            this.Buton_Sil.Size = new System.Drawing.Size(75, 23);
            this.Buton_Sil.TabIndex = 13;
            this.Buton_Sil.Text = "Sil";
            this.Buton_Sil.UseVisualStyleBackColor = true;
            this.Buton_Sil.Click += new System.EventHandler(this.Buton_Sil_Click);
            // 
            // Buton_Güncelle
            // 
            this.Buton_Güncelle.Location = new System.Drawing.Point(713, 68);
            this.Buton_Güncelle.Name = "Buton_Güncelle";
            this.Buton_Güncelle.Size = new System.Drawing.Size(75, 23);
            this.Buton_Güncelle.TabIndex = 14;
            this.Buton_Güncelle.Text = "Güncelle";
            this.Buton_Güncelle.UseVisualStyleBackColor = true;
            this.Buton_Güncelle.Click += new System.EventHandler(this.Buton_Güncelle_Click);
            // 
            // Label_İngilizce
            // 
            this.Label_İngilizce.AutoSize = true;
            this.Label_İngilizce.Location = new System.Drawing.Point(12, 142);
            this.Label_İngilizce.Name = "Label_İngilizce";
            this.Label_İngilizce.Size = new System.Drawing.Size(45, 13);
            this.Label_İngilizce.TabIndex = 16;
            this.Label_İngilizce.Text = "İngilizce";
            // 
            // İngilizce_Var
            // 
            this.İngilizce_Var.AutoSize = true;
            this.İngilizce_Var.Location = new System.Drawing.Point(85, 144);
            this.İngilizce_Var.Name = "İngilizce_Var";
            this.İngilizce_Var.Size = new System.Drawing.Size(15, 14);
            this.İngilizce_Var.TabIndex = 17;
            this.İngilizce_Var.UseVisualStyleBackColor = true;
            // 
            // Label_EkYabancıDil
            // 
            this.Label_EkYabancıDil.AutoSize = true;
            this.Label_EkYabancıDil.Location = new System.Drawing.Point(12, 166);
            this.Label_EkYabancıDil.Name = "Label_EkYabancıDil";
            this.Label_EkYabancıDil.Size = new System.Drawing.Size(107, 13);
            this.Label_EkYabancıDil.TabIndex = 21;
            this.Label_EkYabancıDil.Text = "Ek Yabancı Dil Sayısı";
            // 
            // Text_EkDil
            // 
            this.Text_EkDil.Location = new System.Drawing.Point(127, 163);
            this.Text_EkDil.Name = "Text_EkDil";
            this.Text_EkDil.Size = new System.Drawing.Size(100, 20);
            this.Text_EkDil.TabIndex = 20;
            // 
            // Label_Tip
            // 
            this.Label_Tip.AutoSize = true;
            this.Label_Tip.Location = new System.Drawing.Point(303, 38);
            this.Label_Tip.Name = "Label_Tip";
            this.Label_Tip.Size = new System.Drawing.Size(35, 13);
            this.Label_Tip.TabIndex = 22;
            this.Label_Tip.Text = "İş Tipi";
            // 
            // Box_Tip
            // 
            this.Box_Tip.FormattingEnabled = true;
            this.Box_Tip.Items.AddRange(new object[] {
            "Takım Lideri/Grup Yöneticisi/Teknik Yönetici/Yazılım Mimarı",
            "Proje Yöneticisi",
            "Direktör/Projeler Yöneticisi ",
            "CTO/Genel Müdür",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem Personeli Sayısı <= 5)",
            "Bilgi İşlem Sorumlusu/Müdürü (Bilgi İşlem Personeli Sayısı > 5)"});
            this.Box_Tip.Location = new System.Drawing.Point(376, 34);
            this.Box_Tip.Name = "Box_Tip";
            this.Box_Tip.Size = new System.Drawing.Size(306, 21);
            this.Box_Tip.TabIndex = 23;
            // 
            // Box_Ogrenim
            // 
            this.Box_Ogrenim.FormattingEnabled = true;
            this.Box_Ogrenim.Items.AddRange(new object[] {
            "Meslek alanı ile ilgili lisans",
            "Meslek alanı ile ilgili yüksek lisans",
            "Meslek alanı ile ilgili doktora",
            "Meslek alanı ile ilgili doçentlik",
            "Meslek alanı ile ilgili olmayan yüksek lisans",
            "Meslek alanı ile ilgili olmayan doktora/doçentlik"});
            this.Box_Ogrenim.Location = new System.Drawing.Point(376, 7);
            this.Box_Ogrenim.Name = "Box_Ogrenim";
            this.Box_Ogrenim.Size = new System.Drawing.Size(306, 21);
            this.Box_Ogrenim.TabIndex = 25;
            // 
            // Label_Ogrenim
            // 
            this.Label_Ogrenim.AutoSize = true;
            this.Label_Ogrenim.Location = new System.Drawing.Point(303, 15);
            this.Label_Ogrenim.Name = "Label_Ogrenim";
            this.Label_Ogrenim.Size = new System.Drawing.Size(46, 13);
            this.Label_Ogrenim.TabIndex = 24;
            this.Label_Ogrenim.Text = "Öğrenim";
            // 
            // Label_Evli
            // 
            this.Label_Evli.AutoSize = true;
            this.Label_Evli.Location = new System.Drawing.Point(303, 67);
            this.Label_Evli.Name = "Label_Evli";
            this.Label_Evli.Size = new System.Drawing.Size(43, 13);
            this.Label_Evli.TabIndex = 26;
            this.Label_Evli.Text = "Evli mi?";
            // 
            // Check_Evli_Evet
            // 
            this.Check_Evli_Evet.AutoSize = true;
            this.Check_Evli_Evet.Location = new System.Drawing.Point(376, 66);
            this.Check_Evli_Evet.Name = "Check_Evli_Evet";
            this.Check_Evli_Evet.Size = new System.Drawing.Size(48, 17);
            this.Check_Evli_Evet.TabIndex = 27;
            this.Check_Evli_Evet.Text = "Evet";
            this.Check_Evli_Evet.UseVisualStyleBackColor = true;
            this.Check_Evli_Evet.CheckedChanged += new System.EventHandler(this.Check_Evli_Evet_CheckedChanged);
            // 
            // Label_Cocuk
            // 
            this.Label_Cocuk.AutoSize = true;
            this.Label_Cocuk.Location = new System.Drawing.Point(515, 67);
            this.Label_Cocuk.Name = "Label_Cocuk";
            this.Label_Cocuk.Size = new System.Drawing.Size(68, 13);
            this.Label_Cocuk.TabIndex = 29;
            this.Label_Cocuk.Text = "Çocuk Sayısı";
            // 
            // Label_Cocuk_2
            // 
            this.Label_Cocuk_2.AutoSize = true;
            this.Label_Cocuk_2.Location = new System.Drawing.Point(303, 122);
            this.Label_Cocuk_2.Name = "Label_Cocuk_2";
            this.Label_Cocuk_2.Size = new System.Drawing.Size(166, 13);
            this.Label_Cocuk_2.TabIndex = 33;
            this.Label_Cocuk_2.Text = "En büyük 2.çocuğun doğum tarihi";
            this.Label_Cocuk_2.Visible = false;
            // 
            // Label_Cocuk_1
            // 
            this.Label_Cocuk_1.AutoSize = true;
            this.Label_Cocuk_1.Location = new System.Drawing.Point(303, 96);
            this.Label_Cocuk_1.Name = "Label_Cocuk_1";
            this.Label_Cocuk_1.Size = new System.Drawing.Size(157, 13);
            this.Label_Cocuk_1.TabIndex = 31;
            this.Label_Cocuk_1.Text = "En büyük çocuğun doğum tarihi";
            this.Label_Cocuk_1.Visible = false;
            // 
            // Check_Eşi_Çalışıyor
            // 
            this.Check_Eşi_Çalışıyor.AutoSize = true;
            this.Check_Eşi_Çalışıyor.Location = new System.Drawing.Point(430, 66);
            this.Check_Eşi_Çalışıyor.Name = "Check_Eşi_Çalışıyor";
            this.Check_Eşi_Çalışıyor.Size = new System.Drawing.Size(81, 17);
            this.Check_Eşi_Çalışıyor.TabIndex = 35;
            this.Check_Eşi_Çalışıyor.Text = "Eşi Çalışıyor";
            this.Check_Eşi_Çalışıyor.UseVisualStyleBackColor = true;
            this.Check_Eşi_Çalışıyor.Visible = false;
            // 
            // Buton_Kaydet
            // 
            this.Buton_Kaydet.Location = new System.Drawing.Point(714, 163);
            this.Buton_Kaydet.Name = "Buton_Kaydet";
            this.Buton_Kaydet.Size = new System.Drawing.Size(75, 23);
            this.Buton_Kaydet.TabIndex = 37;
            this.Buton_Kaydet.Text = "Kaydet";
            this.Buton_Kaydet.UseVisualStyleBackColor = true;
            // 
            // Buton_Yükle
            // 
            this.Buton_Yükle.Location = new System.Drawing.Point(714, 132);
            this.Buton_Yükle.Name = "Buton_Yükle";
            this.Buton_Yükle.Size = new System.Drawing.Size(75, 23);
            this.Buton_Yükle.TabIndex = 38;
            this.Buton_Yükle.Text = "Yükle";
            this.Buton_Yükle.UseVisualStyleBackColor = true;
            this.Buton_Yükle.Click += new System.EventHandler(this.Buton_Yükle_Click);
            // 
            // Box_Resim
            // 
            this.Box_Resim.Location = new System.Drawing.Point(12, 247);
            this.Box_Resim.Name = "Box_Resim";
            this.Box_Resim.Size = new System.Drawing.Size(222, 191);
            this.Box_Resim.TabIndex = 39;
            this.Box_Resim.TabStop = false;
            // 
            // Label_Picture
            // 
            this.Label_Picture.AutoSize = true;
            this.Label_Picture.Location = new System.Drawing.Point(12, 220);
            this.Label_Picture.Name = "Label_Picture";
            this.Label_Picture.Size = new System.Drawing.Size(36, 13);
            this.Label_Picture.TabIndex = 40;
            this.Label_Picture.Text = "Resim";
            // 
            // Text_Resim
            // 
            this.Text_Resim.Location = new System.Drawing.Point(54, 217);
            this.Text_Resim.Name = "Text_Resim";
            this.Text_Resim.Size = new System.Drawing.Size(100, 20);
            this.Text_Resim.TabIndex = 41;
            // 
            // Buton_Resim
            // 
            this.Buton_Resim.Location = new System.Drawing.Point(160, 215);
            this.Buton_Resim.Name = "Buton_Resim";
            this.Buton_Resim.Size = new System.Drawing.Size(49, 23);
            this.Buton_Resim.TabIndex = 42;
            this.Buton_Resim.Text = "Ekle";
            this.Buton_Resim.UseVisualStyleBackColor = true;
            this.Buton_Resim.Click += new System.EventHandler(this.Buton_Resim_Click_1);
            // 
            // Buton_Bilgi
            // 
            this.Buton_Bilgi.Location = new System.Drawing.Point(713, 100);
            this.Buton_Bilgi.Name = "Buton_Bilgi";
            this.Buton_Bilgi.Size = new System.Drawing.Size(75, 23);
            this.Buton_Bilgi.TabIndex = 43;
            this.Buton_Bilgi.Text = "Detaylı Bilgi";
            this.Buton_Bilgi.UseVisualStyleBackColor = true;
            this.Buton_Bilgi.Click += new System.EventHandler(this.Buton_Bilgi_Click_1);
            // 
            // Text_Cocuk1_Dogum_Gün
            // 
            this.Text_Cocuk1_Dogum_Gün.Location = new System.Drawing.Point(477, 93);
            this.Text_Cocuk1_Dogum_Gün.Name = "Text_Cocuk1_Dogum_Gün";
            this.Text_Cocuk1_Dogum_Gün.Size = new System.Drawing.Size(32, 20);
            this.Text_Cocuk1_Dogum_Gün.TabIndex = 44;
            this.Text_Cocuk1_Dogum_Gün.Visible = false;
            // 
            // Text_Cocuk1_Dogum_Ay
            // 
            this.Text_Cocuk1_Dogum_Ay.Location = new System.Drawing.Point(515, 93);
            this.Text_Cocuk1_Dogum_Ay.Name = "Text_Cocuk1_Dogum_Ay";
            this.Text_Cocuk1_Dogum_Ay.Size = new System.Drawing.Size(32, 20);
            this.Text_Cocuk1_Dogum_Ay.TabIndex = 45;
            this.Text_Cocuk1_Dogum_Ay.Visible = false;
            // 
            // Text_Cocuk1_Dogum_Yıl
            // 
            this.Text_Cocuk1_Dogum_Yıl.Location = new System.Drawing.Point(553, 93);
            this.Text_Cocuk1_Dogum_Yıl.Name = "Text_Cocuk1_Dogum_Yıl";
            this.Text_Cocuk1_Dogum_Yıl.Size = new System.Drawing.Size(43, 20);
            this.Text_Cocuk1_Dogum_Yıl.TabIndex = 46;
            this.Text_Cocuk1_Dogum_Yıl.Visible = false;
            // 
            // Text_Cocuk2_Dogum_Yıl
            // 
            this.Text_Cocuk2_Dogum_Yıl.Location = new System.Drawing.Point(553, 119);
            this.Text_Cocuk2_Dogum_Yıl.Name = "Text_Cocuk2_Dogum_Yıl";
            this.Text_Cocuk2_Dogum_Yıl.Size = new System.Drawing.Size(43, 20);
            this.Text_Cocuk2_Dogum_Yıl.TabIndex = 49;
            this.Text_Cocuk2_Dogum_Yıl.Visible = false;
            // 
            // Text_Cocuk2_Dogum_Ay
            // 
            this.Text_Cocuk2_Dogum_Ay.Location = new System.Drawing.Point(515, 119);
            this.Text_Cocuk2_Dogum_Ay.Name = "Text_Cocuk2_Dogum_Ay";
            this.Text_Cocuk2_Dogum_Ay.Size = new System.Drawing.Size(32, 20);
            this.Text_Cocuk2_Dogum_Ay.TabIndex = 48;
            this.Text_Cocuk2_Dogum_Ay.Visible = false;
            // 
            // Text_Cocuk2_Dogum_Gün
            // 
            this.Text_Cocuk2_Dogum_Gün.Location = new System.Drawing.Point(477, 119);
            this.Text_Cocuk2_Dogum_Gün.Name = "Text_Cocuk2_Dogum_Gün";
            this.Text_Cocuk2_Dogum_Gün.Size = new System.Drawing.Size(32, 20);
            this.Text_Cocuk2_Dogum_Gün.TabIndex = 47;
            this.Text_Cocuk2_Dogum_Gün.Visible = false;
            // 
            // Box_Cocuk1_Eğitim
            // 
            this.Box_Cocuk1_Eğitim.AutoSize = true;
            this.Box_Cocuk1_Eğitim.Location = new System.Drawing.Point(673, 97);
            this.Box_Cocuk1_Eğitim.Name = "Box_Cocuk1_Eğitim";
            this.Box_Cocuk1_Eğitim.Size = new System.Drawing.Size(15, 14);
            this.Box_Cocuk1_Eğitim.TabIndex = 51;
            this.Box_Cocuk1_Eğitim.UseVisualStyleBackColor = true;
            this.Box_Cocuk1_Eğitim.Visible = false;
            // 
            // Label_Cocuk1_Eğitim
            // 
            this.Label_Cocuk1_Eğitim.AutoSize = true;
            this.Label_Cocuk1_Eğitim.Location = new System.Drawing.Point(613, 97);
            this.Label_Cocuk1_Eğitim.Name = "Label_Cocuk1_Eğitim";
            this.Label_Cocuk1_Eğitim.Size = new System.Drawing.Size(41, 13);
            this.Label_Cocuk1_Eğitim.TabIndex = 50;
            this.Label_Cocuk1_Eğitim.Text = "Okuyor";
            this.Label_Cocuk1_Eğitim.Visible = false;
            // 
            // Box_Cocuk2_Eğitim
            // 
            this.Box_Cocuk2_Eğitim.AutoSize = true;
            this.Box_Cocuk2_Eğitim.Location = new System.Drawing.Point(673, 122);
            this.Box_Cocuk2_Eğitim.Name = "Box_Cocuk2_Eğitim";
            this.Box_Cocuk2_Eğitim.Size = new System.Drawing.Size(15, 14);
            this.Box_Cocuk2_Eğitim.TabIndex = 53;
            this.Box_Cocuk2_Eğitim.UseVisualStyleBackColor = true;
            this.Box_Cocuk2_Eğitim.Visible = false;
            // 
            // Label_Cocuk2_Eğitim
            // 
            this.Label_Cocuk2_Eğitim.AutoSize = true;
            this.Label_Cocuk2_Eğitim.Location = new System.Drawing.Point(613, 122);
            this.Label_Cocuk2_Eğitim.Name = "Label_Cocuk2_Eğitim";
            this.Label_Cocuk2_Eğitim.Size = new System.Drawing.Size(41, 13);
            this.Label_Cocuk2_Eğitim.TabIndex = 52;
            this.Label_Cocuk2_Eğitim.Text = "Okuyor";
            this.Label_Cocuk2_Eğitim.Visible = false;
            // 
            // Box_Cocuk
            // 
            this.Box_Cocuk.Location = new System.Drawing.Point(589, 64);
            this.Box_Cocuk.Name = "Box_Cocuk";
            this.Box_Cocuk.Size = new System.Drawing.Size(38, 20);
            this.Box_Cocuk.TabIndex = 54;
            this.Box_Cocuk.TextChanged += new System.EventHandler(this.Box_Cocuk_TextChanged);
            // 
            // BMO_Progress
            // 
            this.BMO_Progress.Location = new System.Drawing.Point(306, 163);
            this.BMO_Progress.Name = "BMO_Progress";
            this.BMO_Progress.Size = new System.Drawing.Size(393, 23);
            this.BMO_Progress.TabIndex = 55;
            // 
            // BMO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BMO_Progress);
            this.Controls.Add(this.Box_Cocuk);
            this.Controls.Add(this.Box_Cocuk2_Eğitim);
            this.Controls.Add(this.Label_Cocuk2_Eğitim);
            this.Controls.Add(this.Box_Cocuk1_Eğitim);
            this.Controls.Add(this.Label_Cocuk1_Eğitim);
            this.Controls.Add(this.Text_Cocuk2_Dogum_Yıl);
            this.Controls.Add(this.Text_Cocuk2_Dogum_Ay);
            this.Controls.Add(this.Text_Cocuk2_Dogum_Gün);
            this.Controls.Add(this.Text_Cocuk1_Dogum_Yıl);
            this.Controls.Add(this.Text_Cocuk1_Dogum_Ay);
            this.Controls.Add(this.Text_Cocuk1_Dogum_Gün);
            this.Controls.Add(this.Buton_Bilgi);
            this.Controls.Add(this.Buton_Resim);
            this.Controls.Add(this.Text_Resim);
            this.Controls.Add(this.Label_Picture);
            this.Controls.Add(this.Box_Resim);
            this.Controls.Add(this.Buton_Yükle);
            this.Controls.Add(this.Buton_Kaydet);
            this.Controls.Add(this.Check_Eşi_Çalışıyor);
            this.Controls.Add(this.Label_Cocuk_2);
            this.Controls.Add(this.Label_Cocuk_1);
            this.Controls.Add(this.Label_Cocuk);
            this.Controls.Add(this.Check_Evli_Evet);
            this.Controls.Add(this.Label_Evli);
            this.Controls.Add(this.Box_Ogrenim);
            this.Controls.Add(this.Label_Ogrenim);
            this.Controls.Add(this.Box_Tip);
            this.Controls.Add(this.Label_Tip);
            this.Controls.Add(this.Label_EkYabancıDil);
            this.Controls.Add(this.Text_EkDil);
            this.Controls.Add(this.İngilizce_Var);
            this.Controls.Add(this.Label_İngilizce);
            this.Controls.Add(this.Buton_Güncelle);
            this.Controls.Add(this.Buton_Sil);
            this.Controls.Add(this.Label_Tecrübe);
            this.Controls.Add(this.Text_Tecrübe);
            this.Controls.Add(this.Label_CalSehir);
            this.Controls.Add(this.Box_CalSehir);
            this.Controls.Add(this.Label_YasSehir);
            this.Controls.Add(this.Box_YasSehir);
            this.Controls.Add(this.Label_Soyisim);
            this.Controls.Add(this.Text_Soy);
            this.Controls.Add(this.List_Calisanlar);
            this.Controls.Add(this.Button_Ekle);
            this.Controls.Add(this.Label_İsim);
            this.Controls.Add(this.Text_İsim);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BMO";
            this.Text = "BMO Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.Box_Resim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Text_İsim;
        private System.Windows.Forms.Label Label_İsim;
        private System.Windows.Forms.Button Button_Ekle;
        private System.Windows.Forms.ListBox List_Calisanlar;
        private System.Windows.Forms.Label Label_Soyisim;
        private System.Windows.Forms.TextBox Text_Soy;
        private System.Windows.Forms.ComboBox Box_YasSehir;
        private System.Windows.Forms.Label Label_YasSehir;
        private System.Windows.Forms.Label Label_CalSehir;
        private System.Windows.Forms.ComboBox Box_CalSehir;
        private System.Windows.Forms.Label Label_Tecrübe;
        private System.Windows.Forms.TextBox Text_Tecrübe;
        private System.Windows.Forms.Button Buton_Sil;
        private System.Windows.Forms.Button Buton_Güncelle;
        private System.Windows.Forms.Label Label_İngilizce;
        private System.Windows.Forms.CheckBox İngilizce_Var;
        private System.Windows.Forms.Label Label_EkYabancıDil;
        private System.Windows.Forms.TextBox Text_EkDil;
        private System.Windows.Forms.Label Label_Tip;
        private System.Windows.Forms.ComboBox Box_Tip;
        private System.Windows.Forms.ComboBox Box_Ogrenim;
        private System.Windows.Forms.Label Label_Ogrenim;
        private System.Windows.Forms.Label Label_Evli;
        private System.Windows.Forms.CheckBox Check_Evli_Evet;
        private System.Windows.Forms.Label Label_Cocuk;
        private System.Windows.Forms.Label Label_Cocuk_2;
        private System.Windows.Forms.Label Label_Cocuk_1;
        private System.Windows.Forms.CheckBox Check_Eşi_Çalışıyor;
        private System.Windows.Forms.Button Buton_Kaydet;
        private System.Windows.Forms.Button Buton_Yükle;
        private System.Windows.Forms.SaveFileDialog Dosya_Kaydet;
        private System.Windows.Forms.PictureBox Box_Resim;
        private System.Windows.Forms.Label Label_Picture;
        private System.Windows.Forms.TextBox Text_Resim;
        private System.Windows.Forms.Button Buton_Resim;
        private System.Windows.Forms.Button Buton_Bilgi;
        private System.Windows.Forms.TextBox Text_Cocuk1_Dogum_Gün;
        private System.Windows.Forms.TextBox Text_Cocuk1_Dogum_Ay;
        private System.Windows.Forms.TextBox Text_Cocuk1_Dogum_Yıl;
        private System.Windows.Forms.TextBox Text_Cocuk2_Dogum_Yıl;
        private System.Windows.Forms.TextBox Text_Cocuk2_Dogum_Ay;
        private System.Windows.Forms.TextBox Text_Cocuk2_Dogum_Gün;
        private System.Windows.Forms.CheckBox Box_Cocuk1_Eğitim;
        private System.Windows.Forms.Label Label_Cocuk1_Eğitim;
        private System.Windows.Forms.CheckBox Box_Cocuk2_Eğitim;
        private System.Windows.Forms.Label Label_Cocuk2_Eğitim;
        private System.Windows.Forms.TextBox Box_Cocuk;
        private System.Windows.Forms.ProgressBar BMO_Progress;
    }
}

